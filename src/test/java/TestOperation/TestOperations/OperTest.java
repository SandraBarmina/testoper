package TestOperation.TestOperations;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import au.com.bytecode.opencsv.CSVReader;
import junit.framework.TestCase;
import ru.yandex.qatools.allure.annotations.Title;

@Title("Тест арифметических действий")
@RunWith(Parameterized.class)
public class OperTest {
	
public String operand1;
public String operand2;
public String operation;
public String result;

public OperTest(String operand1, String operand2, String operation, String result) {
        this.operand1 = operand1;
        this.operand2 = operand2;
        this.operation = operation;
        this.result = result;
}

@Parameters
public static Collection<Object[]> getTestData() throws IOException {
	CSVReader reader = new CSVReader(new FileReader("resource/file.csv"), ';');
	String[] st = null;
	List<String[]> strMas = new ArrayList<String[]>();
	for (st = reader.readNext();st != null;st = reader.readNext())
		if (st.length == 4)
			strMas.add(st);
		else TestCase.fail("Incorrect number parameters");
	
	Object[][] testParam = new Object [4][strMas.size()];
	for (int i = 0; i < strMas.size(); i++)
		for (int j = 0; j < strMas.size(); j++)
			testParam [i][j] = strMas.get(i)[j];
	return Arrays.asList (testParam);
}

@Title("Тест на операции")
@Test
public void testOperations() {
	
	Integer res=null;
	try {
		if (operation.equals("+"))
			res = Integer.valueOf(operand1) + Integer.valueOf(operand2);
		if (operation.equals("-"))
			res = Integer.valueOf(operand1) - Integer.valueOf(operand2);
		if (operation.equals("/"))
		{
			try {
				res = Integer.valueOf(operand1) / Integer.valueOf(operand2);
			}
			catch (ArithmeticException e) {
				TestCase.fail("/ by zerro");
			}
		}
		if (operation.equals("*"))
			res = Integer.valueOf(operand1) * Integer.valueOf(operand2);
		if (res == null)
			TestCase.fail("Incorrect operation");
		else 
			TestCase.assertTrue("Incorrect calculation result ", res.equals(Integer.parseInt(result)));
	}
	catch (NumberFormatException e) {  
	     TestCase.fail("Incorrect Number format");
	}
}

@After
public void tearDown() {
   
	operand1 = null;
	operand2 = null;
	operation = null;
	result = null;
  }
}
