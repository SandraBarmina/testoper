package TestOperation.TestOperations;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.tika.parser.ParseContext;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import junit.framework.TestCase;

@RunWith(Parameterized.class)
public class Oper {
public static String[] str,str2,str3,str4; 


public String operand1;
public String operand2;
public String operation;
public String result;

public Oper(String operand1, String operand2, String operation, String result) {
        this.operand1 = operand1;
        this.operand2 = operand2;
        this.operation = operation;
        this.result = result;
}

@Parameters
public static Collection<Object[]> getTestData() throws IOException {
	String csvFile = "/home/LDAP/barminaav/Documents/file.csv";  
	  BufferedReader br = null;  
	  String line = " ";  
	  String cvsSplitBy = ";";
	  br = new BufferedReader(new FileReader(csvFile));
	  line = br.readLine();
	  str = line.split(cvsSplitBy);
	  line = br.readLine();
	  str2 = line.split(cvsSplitBy);
	  line = br.readLine();
	  str3 = line.split(cvsSplitBy);
	  line = br.readLine();
	  str4 = line.split(cvsSplitBy);
	return Arrays.asList (new Object[][] {
			{str[0],str[1],str[2],str[3]},
			{str2[0],str2[1],str2[2],str2[3]},
			{str3[0],str3[1],str3[2],str3[3]},
			{str4[0],str4[1],str4[2],str4[3]}
});
}


@Test
public void testSum() {
//    Integer sum = Integer.valueOf(str[0]) + Integer.valueOf(str[1]);
//    assertTrue(sum.equals(Integer.parseInt(str[3])));
	Integer res=0;
	if (operation.equals("+"))
		res = Integer.valueOf(operand1) + Integer.valueOf(operand1);
	if (operation.equals("-"))
		res = Integer.valueOf(operand1) - Integer.valueOf(operand1);
	if (operation.equals("/"))
		res = Integer.valueOf(operand1) / Integer.valueOf(operand1);
	if (operation.equals("*"))
		res = Integer.valueOf(operand1) * Integer.valueOf(operand1);
    TestCase.assertTrue(res.equals(Integer.parseInt(result)));
}

@After
void tearDown() {
   
	operand1 = null;
	operand2 = null;
	operation = null;
	result = null;
  }
}
